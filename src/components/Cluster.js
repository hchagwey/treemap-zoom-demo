import React from "react";
import { Treemap } from "recharts";
import { useNavigate, Link } from "react-router-dom";

const data = [
  {
    name: "Store1",
    size: 1302,
    id: "store-1"
  },
  {
    name: "Store2",
    size: 4500,
    id: "store-2"
  },
  {
    name: "Store3",
    size: 7500,
    id: "store-3"
  },
  {
    name: "Store4",
    size: 3000,
    id: "store-4"
  },
  {
    name: "Store5",
    size: 796,
    id: "store-5"
  },
  {
    name: "Store6",
    size: 2500,
    id: "store-6"
  }
];

export default function Cluster() {
  const navigate = useNavigate();

  const handleClick = e => {
    console.log(e);
    navigate(`${e.id}`);
  };

  return (
    <React.Fragment>
      <Treemap
        width={400}
        height={200}
        data={data}
        dataKey="size"
        ratio={4 / 3}
        stroke="#fff"
        fill="#8884d8"
        onClick={handleClick}
        isAnimationActive={false}
      />
      <Link to="/">Get back to the list of clusters</Link>
    </React.Fragment>
  );
}
