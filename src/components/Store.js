import React from "react";
import { Treemap } from "recharts";
import { Link, useParams } from "react-router-dom";

const data = [
  {
    name: "product1",
    size: 1302,
    id: "product-1"
  },
  {
    name: "product2",
    size: 4500,
    id: "product-2"
  },
  {
    name: "product3",
    size: 7500,
    id: "product-3"
  },
  {
    name: "product4",
    size: 3000,
    id: "product-4"
  },
  {
    name: "product5",
    size: 796,
    id: "product-5"
  },
  {
    name: "product6",
    size: 6500,
    id: "product-6"
  },
  {
    name: "product7",
    size: 9300,
    id: "product-7"
  },
  {
    name: "product8",
    size: 3602,
    id: "product-8"
  },
  {
    name: "product9",
    size: 1369,
    id: "product-9"
  }
];

export default function Cluster() {
  let { clusterId } = useParams();

  return (
    <React.Fragment>
      <Treemap
        width={400}
        height={200}
        data={data}
        dataKey="size"
        ratio={4 / 3}
        stroke="#fff"
        fill="#8884d8"
        isAnimationActive={false}
      />
      <Link to="/">Get back to the list of clusters</Link>
      <br />
      <Link to={`/${clusterId}`}>Get back to the list of stores</Link>
    </React.Fragment>
  );
}
