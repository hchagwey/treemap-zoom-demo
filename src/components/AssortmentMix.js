import React from "react";
import { Treemap } from "recharts";
import { useNavigate } from "react-router-dom";

const data = [
  {
    name: "Cluster1",
    size: 1302,
    id: "cluster-1"
  },
  {
    name: "Cluster2",
    size: 4500,
    id: "cluster-2"
  },
  {
    name: "Cluster3",
    size: 2500,
    id: "cluster-3"
  }
];

export default function AssortmentMix() {
  const navigate = useNavigate();

  const handleClick = e => {
    navigate(`${e.id}`);
  };

  return (
    <React.Fragment>
      <Treemap
        width={400}
        height={200}
        data={data}
        dataKey="size"
        ratio={4 / 3}
        stroke="#fff"
        fill="#8884d8"
        onClick={handleClick}
        isAnimationActive={false}
      />
    </React.Fragment>
  );
}
