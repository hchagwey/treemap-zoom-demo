import React from "react";
import AssortmentMix from "./components/AssortmentMix";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Cluster from "./components/Cluster";
import Store from "./components/Store";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<AssortmentMix />} />
        <Route path=":clusterId" element={<Cluster />} />
        <Route path=":clusterId/:storeId" element={<Store />} />
      </Routes>
    </Router>
  );
}

export default App;
